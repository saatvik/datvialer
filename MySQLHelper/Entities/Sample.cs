﻿using MySQLHelper.Global;
using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;

namespace MySQLHelper.Entities
{
    public class Sample : Entity<Sample>
    {
        [Identity]
        public int SampleID { get; set; }
        public string SampleName { get; set; }
        public string ParameterName { get; set; }
        public string Status { get; set; }
        public int TriggerNumber { get; set; }
        public decimal ParameterValue { get; set; }
        public int WorkStationNo { get; set; }
        public string ProgramName { get; set; }

        public int IsError { get; set; }

        public void Add()
        {
            try
            {
                base.Add();
            }
            catch { }
        }

        public List<Sample> GetSamples(DateTime startTime, DateTime endDateTime, string programName)
        {
            List<Sample> samples = new List<Sample>();
            try
            {
                var sSQL = "SELECT * FROM Sample WHERE IsError = 0 AND ProgramName = '" + programName + "'";
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).Where(s => s.ModifiedOn >= startTime && s.ModifiedOn <= endDateTime).ToList();
            }
            catch { }
            return samples;
        }

        public List<Sample> GetSamples(string sampleName)
        {
            List<Sample> samples = new List<Sample>();
            try
            {
                var sSQL = "SELECT * FROM Sample WHERE SampleName = '" + sampleName + "'";
                samples = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).ToList();
            }
            catch { }
            return samples;
        }


        public void UpdteSamplesAsError(string sampleName)
        {
            try
            {
                string sSQL = "SELECT * FROM Sample WHERE SampleName = '" + sampleName + "'";
                var data = ExecuteDataTable(sSQL);

                if (data.AsEnumerable().Any(dr => Convert.ToInt32(dr.Field<int>("IsError")) == 1))
                {
                    sSQL = "UPDATE Sample Set IsError = 1 WHERE SampleName = '" + sampleName + "'";
                    ExecuteNonQuery(sSQL);
                }
            }
            catch { }
        }

        public Sample GetLastSample(string programName)
        {
            Sample sample = new Sample();
            try
            {
                var sSQL = "SELECT* FROM Sample WHERE ProgramName = '" + programName + "' ORDER BY sampleid DESC";
                sample = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).FirstOrDefault();
            }
            catch { }
            return sample;
        }

        public List<Sample> GetSamples(DataTable dtData)
        {
            return dtData.AsEnumerable().Select(dr => Get(dr)).ToList();
        }

        //Delete all previously generated samples data
        public void ResetSQLData(string programName)
        {
            try
            {
                string sSQL = "DELETE FROM Sample WHERE ProgramName = '" + programName + "'";
                ExecuteNonQuery(sSQL);
            }
            catch { }
        }
    }



    public class ParameterValue : Entity<ParameterValue>
    {
        [Identity]
        public int ParameterValueID { get; set; }
        public int SampleID { get; set; }
        public decimal Value { get; set; }

        public void AddParameterValue()
        {
            base.Add();
        }

        public List<ParameterValue> GetParamValues(List<int> sampleIDs)
        {
            List<ParameterValue> paramValues = new List<ParameterValue>();
            try
            {
                var sSQL = "SELECT * FROM ParameterValue";
                paramValues = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).Where(s => sampleIDs.Contains(s.SampleID)).ToList();
            }
            catch { }
            return paramValues;
        }


    }
}
