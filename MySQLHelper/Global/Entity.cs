﻿using MySQLHelper.Globals;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace MySQLHelper.Global
{
    public class Entity<T> : SQLHelper
    {
        public DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }

        protected virtual int Add()
        {
            this.ModifiedOn = DateTime.Now;

            List<string> names = new List<string>();
            List<string> values = new List<string>();
            PropertyInfo[] properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (property.GetCustomAttributes(true).Any(n => n.GetType() == typeof(Identity)))
                {
                    continue;
                }

                if (property.GetCustomAttributes(true).Any(n => n.GetType() == typeof(Exclude)))
                {
                    continue;
                }

                names.Add(property.Name);
                var value = property.GetValue(this, null);

                if (value != null && value.GetType() == typeof(Boolean))
                {
                    values.Add(value.GetHashCode().ToString());
                }
                else if (value != null && value.GetType() == typeof(DateTime))
                {
                    values.Add(string.Format("{0:s}", value));
                }
                else
                {
                    values.Add(GetValue<object>(property).ToString());
                }
            }

            string sSQL = "INSERT INTO " + this.GetType().Name + "(" + string.Join(",", names) + ") VALUES(";
            for (int i = 0; i < names.Count; i++)
            {
                sSQL += "'" + (values[i].Equals(string.Empty) ? string.Empty : values[i]) + "',";
            }
            sSQL = sSQL.TrimEnd(',');
            sSQL += ")";

            return ExecuteNonQuery(sSQL);
        }

        protected virtual void Update()
        {
            List<string> names = new List<string>();
            List<string> values = new List<string>();
            PropertyInfo[] properties = this.GetType().GetProperties();
            PropertyInfo identity = null;
            object identityValue = 0;
            foreach (var property in properties)
            {
                names.Add(property.Name);
                var value = property.GetValue(this, null);
                if (identity == null && property.GetCustomAttributes(true).Any(n => n.GetType() == typeof(Identity)))
                {
                    identity = property;
                    identityValue = value;
                    if ((int)identityValue == 0)
                    {
                        Add();
                        break;
                    }
                }

                if (value != null && value.GetType() == typeof(Boolean))
                {
                    values.Add(value.GetHashCode().ToString());
                }
                else if (value != null && value.GetType() == typeof(DateTime))
                {
                    values.Add(string.Format("{0:s}", value));
                }
                else
                {
                    values.Add(GetValue<object>(property).ToString());
                }
            }

            string sSQL = "UPDATE " + this.GetType().Name + " SET ";
            for (int i = 0; i < names.Count; i++)
            {
                sSQL += names[i] + "=" + "'" + (values[i].Equals(string.Empty) ? string.Empty : values[i]) + "',";
            }
            sSQL = sSQL.TrimEnd(',');
            sSQL += " WHERE " + identity.Name + "=" + identityValue.ToString();
            ExecuteNonQuery(sSQL);
        }

        protected virtual object GetValue(PropertyInfo property)
        {
            return property.GetValue(this, null);
        }

        private R GetValue<R>(PropertyInfo property)
        {
            if (property.PropertyType == typeof(byte[]))
            {
                return (R)GetValue(property);
            }

            object value = null;
            if (property.DeclaringType == typeof(Entity<T>))
                value = property.GetValue(this, null);
            else
                value = GetValue(property);
            if (value == null)
                if (property.PropertyType == typeof(string))
                    return (R)Convert.ChangeType("", typeof(R));
                else
                    return default(R);

            return (R)Convert.ChangeType(value, typeof(R));
        }

        protected virtual T Get(DataRow row)
        {
            T obj = (T)Activator.CreateInstance(typeof(T));
            return Get(row, obj);
        }

        protected virtual T Get(DataRow row, T obj)
        {
            PropertyInfo[] properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(true);
                string fieldName = property.Name;
                if (row.Table.Columns.Contains(fieldName))
                {
                    try
                    {
                        property.SetValue(obj, row[fieldName], null);
                    }
                    catch { }
                }
            }
            return obj;
        }
    }



    public class Identity : System.Attribute
    {
    }

    public class Exclude : System.Attribute
    {
    }
}