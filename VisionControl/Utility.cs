using System;
using System.Windows.Forms;
using System.Resources;

using Cognex.VisionPro;
using Cognex.VisionPro.Implementation.Internal;
using Cognex.VisionPro.QuickBuild;
using System.Net.NetworkInformation;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using OfficeOpenXml;
using System.IO;
using System.Xml;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using MySQLHelper.Entities;

namespace VisionControl
{
    public class Utility
    {
        public static AccessLevel CurrentAccessLevel = AccessLevel.Operator;
        private static int sampleCount = 0;
        static public ICogRecord TraverseSubRecords(ICogRecord r, string[] subs)
        {
            // Utility function to walk down to a specific subrecord
            if (r == null)
                return r;

            foreach (string s in subs)
            {
                if (r.SubRecords.ContainsKey(s))
                    r = r.SubRecords[s];
                else
                    return null;
            }

            return r;
        }

        static public void FlushAllQueues(CogJobManager jm)
        {
            // Flush all queues
            if (jm == null)
                return;

            jm.UserQueueFlush();
            jm.FailureQueueFlush();
            for (int i = 0; i < jm.JobCount; i++)
            {
                jm.Job(i).OwnedIndependent.RealTimeQueueFlush();
                jm.Job(i).ImageQueueFlush();
            }
        }

        static public int GetJobIndexFromName(CogJobManager mgr, string name)
        {
            if (mgr != null)
            {
                for (int i = 0; i < mgr.JobCount; ++i)
                    if (mgr.Job(i).Name == name)
                        return i;
            }
            return -1;
        }

        static public bool AddRecordToDisplay(CogRecordsDisplay disp, ICogRecord r, string[] subs,
          bool pickBestImage)
        {
            // Utility function to put a specific subrecord into a display
            ICogRecord addrec = Utility.TraverseSubRecords(r, subs);
            if (addrec != null)
            {
                // if this is the first record in, then always select an image
                if (disp.Subject == null)
                    pickBestImage = true;

                disp.Subject = addrec;

                if (pickBestImage)
                {
                    // select first non-empty image record, to workaround the fact that the input image tool
                    // adds an empty subrecord to the LastRun record when it is disabled (when an image file
                    // tool is used, for example)
                    for (int i = 0; i < addrec.SubRecords.Count; ++i)
                    {
                        ICogImage img = addrec.SubRecords[i].Content as ICogImage;
                        if (img != null && img.Height != 0 && img.Width != 0)
                        {
                            disp.SelectedRecordKey = addrec.RecordKey + "." + addrec.SubRecords[i].RecordKey;
                            break;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        private static bool TypeIsNumeric(Type t)
        {
            if (t == null)
                return false;

            if (t == typeof(double) ||
              t == typeof(long) ||
              t == typeof(sbyte) || t == typeof(byte) ||
              t == typeof(short) || t == typeof(ushort) ||
              t == typeof(int) || t == typeof(uint) ||
              t == typeof(ulong))
                return true;

            return false;
        }

        private static Type GetPropertyType(object obj, string path)
        {
            if (obj == null || path == "")
                return null;

            System.Reflection.MemberInfo[] infos = CogToolTerminals.
              ConvertPathToMemberInfos(obj, obj.GetType(), path);

            if (infos.Length == 0)
                return null;

            // Return the type of the last path element.
            return CogToolTerminals.GetReturnType(infos[infos.Length - 1]);
        }

        public static void FillUserResultData(Control ctrl, ICogRecord result, string path)
        {
            FillUserResultData(ctrl, result, path, false);
        }

        public static void FillUserResultData(Control ctrl, ICogRecord result, string path, bool convertRadiansToDegrees)
        {
            // Extract the data identified by the path (if available) from the given result record.
            // Use a format string for doubles.
            string rtn;
            HorizontalAlignment align = HorizontalAlignment.Left;
            if (result == null)
                rtn = ResourceUtility.GetString("RtResultNotAvailable");
            else
            {
                object obj = null;

                try
                {
                    obj = result.SubRecords[path].Content;
                }
                catch
                {
                }

                // check if data is available
                if (obj != null && obj.GetType().FullName != "System.Object")
                {
                    if (obj.GetType() == typeof(double))
                    {
                        double d = (double)obj;
                        if (convertRadiansToDegrees)
                            d = CogMisc.RadToDeg(d);
                        rtn = d.ToString("0.000");
                    }
                    else
                        rtn = obj.ToString();

                    if (TypeIsNumeric(obj.GetType()))
                        align = HorizontalAlignment.Right;
                }
                else
                    rtn = ResourceUtility.GetString("RtResultNotAvailable");
            }

            ctrl.Text = rtn;
            TextBox box = ctrl as TextBox;
            if (box != null)
                box.TextAlign = align;
        }

        public static void SetupPropertyProvider(CogToolPropertyProvider p, Control gui, object tool, string path)
        {
            p.SetPath(gui, path);

            TextBox box = gui as TextBox;
            if (box != null)
            {
                Type t = GetPropertyType(tool, path);
                if (TypeIsNumeric(t))
                    box.TextAlign = HorizontalAlignment.Right;
            }
        }

        public static string GetThisExecutableDirectory()
        {
            string loc = Application.ExecutablePath;
            loc = System.IO.Path.GetDirectoryName(loc) + "\\";
            return loc;
        }

        public static bool AccessAllowed(string stringLevelRequired, AccessLevel currentLogin)
        {
            // return true if the currentLogin is equal to or greater than the given access
            // level (expressed as a string)
            AccessLevel needed = AccessLevel.Administrator;

            try
            {
                object obj = Enum.Parse(typeof(AccessLevel), stringLevelRequired, true);
                needed = (AccessLevel)obj;
            }
            catch (ArgumentException)
            {
            }

            return currentLogin >= needed;
        }

        /// <summary>
        /// Take a filename (generally a relative path) and determine the full path to the file to
        /// use.  First the directory containing the current .vpp file is checked for the given filename,
        /// then the directory containing this code's assembly is checked.
        /// </summary>
        public static string ResolveAssociatedFilename(string vppfname, string fname)
        {
            // check for the given file in the same directory as the developer vpp file path
            string trydev = System.IO.Path.GetDirectoryName(vppfname) + "\\" + fname;
            if (System.IO.File.Exists(trydev))
            {
                fname = trydev;
            }
            else
            {
                // otherwise use same directory as this executable
                fname = GetThisExecutableDirectory() + fname;
            }

            return fname;
        }

        public static string GetUserResultData(ICogRecord result, bool convertRadiansToDegrees)
        {
            string rtn;

            if (result == null)
                rtn = ResourceUtility.GetString("RtResultNotAvailable");
            else
            {
                object obj = null;

                try
                {
                    obj = result.Content;
                }
                catch
                {
                }

                // check if data is available
                if (obj != null && obj.GetType().FullName != "System.Object")
                {
                    if (obj.GetType() == typeof(double))
                    {
                        double d = (double)obj;
                        if (convertRadiansToDegrees)
                            d = CogMisc.RadToDeg(d);
                        rtn = d.ToString("0.000");
                    }
                    else
                        rtn = obj.ToString();
                }
                else
                    rtn = ResourceUtility.GetString("RtResultNotAvailable");
            }

            return rtn;
        }

        public static bool ValidateMacAddress()
        {
            return true;
            var licencedMacAddressess = new List<string>() { "D4BED930340A", "otherMacID1", "otherMacID2" };
            bool IsMacValid = false;

            string systemMacAddress = NetworkInterface.GetAllNetworkInterfaces()
                        .Where(n => !string.IsNullOrEmpty(n.GetPhysicalAddress().ToString())
                            && n.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                        .OrderBy(n => n.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                        .Select(n => n.GetPhysicalAddress().ToString())
                        .FirstOrDefault();

            foreach (var licencedMacAddress in licencedMacAddressess)
            {
                if (!string.IsNullOrEmpty(licencedMacAddress) && !string.IsNullOrEmpty(systemMacAddress) && licencedMacAddress.Equals(systemMacAddress))
                {
                    IsMacValid = true;
                    break;
                }
            }


            if (!IsMacValid)
            {
                var dialogResult = MessageBox.Show("Invalid Licence", "ERROR", MessageBoxButtons.OK);
            }

            return IsMacValid;
        }

        public static MemoryStream GetExcelMemoryStream(DataTable data)
        {
            int starting_Row = 8;
            MemoryStream ms = new MemoryStream();
            try
            {
                using (FileStream fs = File.OpenRead(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int serialNumber = 1;
                        excelWorksheet.Cells["B1:C1"].Merge = true;
                        excelWorksheet.Cells[1, 2].Value = string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());
                        foreach (DataRow row in data.Rows)
                        {
                            excelWorksheet.Cells[starting_Row, 2].Value = serialNumber.ToString();
                            excelWorksheet.Cells[starting_Row, 3].Value = row["ProgramName"].ToString();
                            excelWorksheet.Cells[starting_Row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            excelWorksheet.Cells[starting_Row, 4].Value = row["Dimension"].ToString();
                            excelWorksheet.Cells[starting_Row, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            excelWorksheet.Cells[starting_Row, 5].Value = row["Limit"].ToString();
                            excelWorksheet.Cells[starting_Row, 5].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            excelWorksheet.Cells[starting_Row, 6].Value = row["Result"].ToString();
                            excelWorksheet.Cells[starting_Row, 7].Value = row["Status"].ToString();
                            excelWorksheet.Cells[starting_Row, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            serialNumber++;
                            starting_Row++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }
            }
            catch (Exception ex)
            {

                return ms;
            }

            return ms;

        }

        private static void creatXMLFile(string defaultPath)
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);
            XmlElement element1 = doc.CreateElement("ResultPath", "");
            element1.InnerText = defaultPath;
            doc.AppendChild(element1);
            doc.Save(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "\\settings.xml"));
        }

        public static string GetResultPath()
        {
            var resultPath = "";
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + "\\settings.xml");
                resultPath = doc.SelectSingleNode("ResultPath").InnerText;
            }
            catch (Exception ex)
            {
                var defaultPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Results");
                creatXMLFile(defaultPath);
                return defaultPath;
            }
            return resultPath;
        }

        public static int workStationCount = 1;
        public static void SetResultPath(string resultPath)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + "\\settings.xml");
                doc.SelectSingleNode("ResultPath").InnerText = resultPath;
                doc.Save(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "settings.xml"));
            }
            catch (Exception ex)
            {
                var defaultPath = Path.Combine(resultPath);
                creatXMLFile(defaultPath);
            }
        }

        public void AddRecordsToDB(DataTable dtData)
        {
            if (dtData.AsEnumerable().Any(dr => dr.Field<String>("IsError") != null && dr.Field<String>("IsError").Equals("1")))
            {
                return;
            }

            var reportSetting = new ReportSetting().GetReportSetting();
            foreach (DataRow dr in dtData.Rows)
            {
                MySQLHelper.Entities.Sample sample = new MySQLHelper.Entities.Sample();
                sample.ParameterName = dr.Field<string>("ParameterName");
                sample.SampleName = dr.Field<string>("SampleName");
                sample.ProgramName = dr.Field<string>("ProgramName");
                sample.ParameterValue = Convert.ToDecimal(dr.Field<decimal>("ParameterValue"));
                sample.Status = dr.Field<string>("Status");
                sample.TriggerNumber = dr.Field<int>("TrigerNumber");
                sample.WorkStationNo = workStationCount;
                sample.IsError = Convert.ToInt32(dr.Field<string>("IsError"));

                var lastSample = new Sample().GetLastSample(sample.SampleName);
                if (lastSample != null)
                {
                    if (lastSample.TriggerNumber == 4  && lastSample.TriggerNumber != sample.TriggerNumber)
                    {
                        if (reportSetting.ReportGenerationType == ReportSetting.ReportGenerationTypes.EveryTrigger.GetHashCode())
                        {
                            var fileNameParams = lastSample.SampleName.Split('_');
                            //Get the program name by removing the trailing number.
                            new Utility().GenerateReport(fileNameParams.Count() > 1 ? fileNameParams[0] : lastSample.SampleName, new DateTime(1900, 01, 01), new DateTime(2020, 01, 01), new Sample().GetSamples(lastSample.SampleName));
                        }
                        sampleCount++;
                        workStationCount += 1;
                        sample.WorkStationNo = workStationCount;
                    }
                }
                sample.SampleName = sample.SampleName + "_" + sampleCount;
                sample.Add();
            }
        }

        public void GenerateReport(string programName, DateTime startTime, DateTime endTime, List<Sample> samples = null)
        {
            if (startTime == null)
            {
                startTime = new DateTime(1900, 01, 01);
            }

            if (endTime == null)
            {
                endTime = new DateTime(2020, 01, 01);
            }

            if (samples == null)
                samples = new MySQLHelper.Entities.Sample().GetSamples(startTime, endTime, programName);
            var pNames = samples.Select(s => s.ParameterName).Distinct().ToList();
            var paramAttributes = new ParamAttribute().GetAll();

            List<string> parameterNames = new List<string>();
            pNames.ForEach(p =>
            {
                var pA = paramAttributes.Find(pAA => pAA.ParamName.Equals(p));
                if (pA != null && pA.IncludeInReport)
                {
                    parameterNames.Add(p);
                }
            });

            //Headers
            DataTable dtReportData = new DataTable();
            dtReportData.Columns.Add("SampleName", typeof(string));
            parameterNames.ForEach(pName =>
            {
                dtReportData.Columns.Add(pName, typeof(DataTable));
            });

            var grpData = (from s in samples
                           group s by new
                           {
                               s.SampleName,
                               s.ParameterName,
                           } into g
                           select new ReportData
                           {
                               SampleName = g.Key.SampleName,
                               ParamName = g.Key.ParameterName,
                               ParamValues = g.Where(p => p.ParameterValue != 0).ToList().Select(s => s.ParameterValue).ToList(),
                               Triggers = g.ToList()
                           }).ToList();

            var sampleNames = samples.Select(s => s.SampleName).Distinct().ToList();
            sampleNames.ForEach(s =>
            {
                var sampleDataList = grpData.FindAll(g => g.SampleName.Equals(s)).ToList();
                var row = dtReportData.NewRow();
                row["SampleName"] = sampleDataList.First().SampleName;
                sampleDataList.ForEach(sampleData =>
                {
                    if (parameterNames.Contains(sampleData.ParamName))
                    {
                        var triggers = sampleData.Triggers;
                        var dtValues = new DataTable();
                        for (var i = 1; i <= triggers.Count; i++)
                        {
                            dtValues.Columns.Add(i.ToString(), typeof(DataTable));
                        }

                        var newRow = dtValues.NewRow();
                        for (var i = 1; i <= triggers.Count; i++)
                        {
                            DataTable valueData = new DataTable();
                            valueData.Columns.Add("Value" + i, typeof(decimal));
                            valueData.Columns.Add("Status" + i, typeof(int));

                            var valRow = valueData.NewRow();
                            valRow["Value" + i] = sampleData.ParamValues[i - 1];
                            valRow["Status" + i] = string.IsNullOrEmpty(triggers[i - 1].Status) ? -1 : (triggers[i - 1].Status.ToLower().Equals("pass") ? 1 : 0);
                            valueData.Rows.Add(valRow);
                            valueData.AcceptChanges();
                            newRow[i.ToString()] = valueData;
                        }
                        dtValues.Rows.Add(newRow);
                        dtValues.AcceptChanges();
                        row[sampleData.ParamName] = dtValues;
                    }
                });
                dtReportData.Rows.Add(row);
            });

            PdfPTable mTable = new PdfPTable(parameterNames.Count + 1);
            mTable.HorizontalAlignment = 1;
            //mTable.LockedWidth = true;
            //mTable.TotalWidth = 500f;
            //mTable.SplitRows = true;
            //mTable.SetWidths(new float[] { 63f, 63f, 63f, 63f, 63f, 63f, 63f, 63f });
            mTable.WidthPercentage = 130f;
            //Main Header
            foreach (var col in dtReportData.Columns)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9f, 1);
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                {
                    var newCell = new PdfPCell(new Phrase("Identifier", font));
                    newCell.HorizontalAlignment = 1;
                    mTable.AddCell(newCell);
                }
                else if (!column.Equals("TriggerNumber"))
                    mTable.AddCell(new Phrase(column.ToString(), font));
            }

            // Header: Nominal
            //PdfPRow row = new PdfPRow()
            PdfPCell[] cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("Nominal", font));
                else
                {
                    var val = paramAttributes.Where(p => p.AttributeName.Equals("Nominal")).ToList().Find(a => a.ParamName.Equals(column.ToString()));
                    cells[i] = new PdfPCell(new Phrase(val != null ? val.AttributeValue.ToString() : "", font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            // Header: +Tolerance
            cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("+ Tolerance", font));
                else
                {
                    var val = paramAttributes.Where(p => p.AttributeName.Equals("PTolerence")).ToList().Find(a => a.ParamName.Equals(column.ToString()));
                    cells[i] = new PdfPCell(new Phrase(val != null ? val.AttributeValue.ToString() : "", font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            // Header: -Tolerance
            cells = new PdfPCell[parameterNames.Count + 1];
            for (var i = 0; i < dtReportData.Columns.Count; i++)
            {
                Font font = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8f, 0);
                var col = dtReportData.Columns[i];
                var column = col.GetType().GetProperty("ColumnName").GetValue(col, null);
                if (column.Equals("SampleName"))
                    cells[i] = new PdfPCell(new Phrase("- Tolerance", font));
                else
                {
                    var val = paramAttributes.Where(p => p.AttributeName.Equals("NTolerence")).ToList().Find(a => a.ParamName.Equals(column.ToString()));
                    cells[i] = new PdfPCell(new Phrase(val != null ? val.AttributeValue.ToString() : "", font));
                }
            }
            mTable.Rows.Add(new PdfPRow(cells));

            Font rowFont = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6f, 0);
            Font rowFontRed = new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 7f, 0, BaseColor.RED);
            for (var k = 0; k < dtReportData.Rows.Count; k++)
            {
                int count = 0;
                var icells = new PdfPCell[parameterNames.Count + 1];
                DataRow dr = dtReportData.Rows[k];
                icells[count++] = new PdfPCell(new Phrase(dr["SampleName"].ToString(), rowFont));
                parameterNames.ForEach(p =>
                {
                    DataTable dtValues = null;
                    try
                    {
                        dtValues = (DataTable)dr[p];
                    }
                    catch { }
                    PdfPTable iTable = null;
                    if (dtValues != null && dtValues.Columns.Count != 0)
                    {
                        iTable = new PdfPTable(dtValues.Columns.Count + 2);
                        //iTable.WidthPercentage = 30f;
                        for (var i = 1; i <= dtValues.Columns.Count; i++)
                        {
                            iTable.AddCell(new Phrase(i.ToString(), rowFont));
                        }
                        iTable.AddCell(new Phrase("AVG", rowFont));
                        iTable.AddCell(new Phrase("Status", rowFont));

                        PdfPCell[] iCells = new PdfPCell[dtValues.Columns.Count + 2];
                        decimal sum = 0;
                        int passCount = 0;
                        bool isBlank = false;
                        for (var i = 1; i <= dtValues.Columns.Count; i++)
                        {
                            var vData = dtValues.Rows[0].Field<DataTable>(i.ToString());
                            var value = vData.Rows[0].Field<decimal>("Value" + i);
                            var isPass = vData.Rows[0].Field<int>("Status" + i) == 1;
                            if (!isBlank && vData.Rows[0].Field<int>("Status" + i) == -1)
                            {
                                isBlank = true;
                            }
                            passCount += isPass ? 1 : 0;
                            sum += value;
                            iCells[i - 1] = new PdfPCell(new Phrase(value.ToString(), (isBlank || isPass) ? rowFont : rowFontRed));
                        }
                        iCells[dtValues.Columns.Count] = new PdfPCell(new Phrase((Math.Round(sum / dtValues.Columns.Count, 3)).ToString(), rowFont));
                        iCells[dtValues.Columns.Count + 1] = new PdfPCell(new Phrase(passCount == dtValues.Columns.Count ? "Pass" : (isBlank ? "" : "Fail"), rowFont));
                        iTable.Rows.Add(new PdfPRow(iCells));
                    }
                    icells[count++] = iTable != null ? new PdfPCell(iTable) : new PdfPCell(new Phrase(""));
                });
                mTable.Rows.Add(new PdfPRow(icells));
            }

            Document document = new Document(new RectangleReadOnly(595, 842, 90), 88f, 88f, 10f, 10f);
            var productionParams = new ProductionParam().GetProductionParams(programName);
            if (productionParams == null)
            {
                productionParams = new ProductionParam();
            }
            ReportSetting rp = new ReportSetting().GetReportSetting();
            var filePath = (rp != null ? rp.ReportFilePath.Replace('|', '\\') : VisionControl.mResultsPath) + (@"\" + productionParams.ProductName + "_" + productionParams.BatchNumber + "_" + string.Format("{0:yyyyMMddHHmmss}", DateTime.Now)) + ".pdf";
            var instance = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));
            document.Open();

            var customer = new PdfPTable(3);
            customer.WidthPercentage = 110f;
            var eCell = new PdfPCell(new Phrase("           "));
            eCell.Border = 0;
            var customerNameCell = new PdfPCell(new Phrase(productionParams.CustomerName, new Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10f, 1)));
            customerNameCell.HorizontalAlignment = 2;
            customerNameCell.Border = 0;
            customer.AddCell(eCell);
            customer.AddCell(eCell);
            customer.AddCell(customerNameCell);
            document.Add(customer);

            //Production parameters information
            var productParams = new PdfPTable(1);
            productParams.HorizontalAlignment = 1;
            productParams.WidthPercentage = 130f;
            var timeStampCell = new PdfPCell(new Phrase(String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now), rowFont));
            var pCell = new PdfPCell(new Phrase("Product Name:  " + productionParams.ProductName, rowFont));
            pCell.Border = 0;
            var vCell = new PdfPCell(new Phrase("Variant:  " + productionParams.Variant, rowFont));
            vCell.Border = 0;
            var bCell = new PdfPCell(new Phrase("Batch No:  " + productionParams.BatchNumber, rowFont));
            bCell.Border = 0;
            var lCell = new PdfPCell(new Phrase("Lot No:  " + productionParams.LotNumber, rowFont));
            lCell.Border = 0;

            //Remarks Section
            var remarks = new PdfPTable(1);
            remarks.HorizontalAlignment = 1;
            remarks.WidthPercentage = 130f;
            var emptyCell = new PdfPCell(new Phrase(" "));
            emptyCell.Border = 0;
            var checkedByCell = new PdfPCell(new Phrase("Checked By:  ", rowFont));
            checkedByCell.Border = 0;
            var approvedByCell = new PdfPCell(new Phrase("Approved By:  ", rowFont));
            approvedByCell.Border = 0;
            var remarksCell = new PdfPCell(new Phrase("Remarks:  ", rowFont));
            remarksCell.Border = 0;
            remarks.AddCell(emptyCell);
            remarks.AddCell(checkedByCell);
            remarks.AddCell(approvedByCell);
            remarks.AddCell(remarksCell);

            timeStampCell.Border = 0;
            timeStampCell.HorizontalAlignment = 2;
            productParams.AddCell(pCell);
            productParams.AddCell(vCell);
            productParams.AddCell(bCell);
            productParams.AddCell(lCell);
            productParams.AddCell(timeStampCell);
            document.Add(productParams);
            document.Add(mTable);

            document.Add(remarks);
            document.Close();

        }

        public void SetAttrributeValues(Dictionary<string, Attributes> attributeDictionary)
        {
            new ParamAttribute().DeleteAll();
            foreach (var p in attributeDictionary)
            {
                new ParamAttribute() { ParamName = p.Value.PName, AttributeName = "Nominal", AttributeValue = p.Value.Nominal, IncludeInReport = p.Value.IncludeInReport }.Add();
                new ParamAttribute() { ParamName = p.Value.PName, AttributeName = "PTolerence", AttributeValue = p.Value.PTolerence, IncludeInReport = p.Value.IncludeInReport }.Add();
                new ParamAttribute() { ParamName = p.Value.PName, AttributeName = "NTolerence", AttributeValue = p.Value.NTolerence, IncludeInReport = p.Value.IncludeInReport }.Add();
            }
        }
    }

    public class ResourceUtility
    {
        // helper class to wrap string resources for this application

        private static ResourceManager mResources;

        static ResourceUtility()
        {
            mResources = new ResourceManager("VisionControl.strings",
              System.Reflection.Assembly.GetExecutingAssembly());
        }

        public static string GetString(string resname)
        {
            string str = mResources.GetString(resname);
            if (str == null)
                str = "ERROR(" + resname + ")";
            return str;
        }

        public static string FormatString(string resname, string arg0)
        {
            try
            {
                return string.Format(GetString(resname), arg0);
            }
            catch (Exception)
            {
            }

            return "ERROR(" + resname + ")";
        }

        public static string FormatString(string resname, string arg0, string arg1)
        {
            try
            {
                return string.Format(GetString(resname), arg0, arg1);
            }
            catch (Exception)
            {
            }

            return "ERROR(" + resname + ")";
        }
    }

    public class ReportData
    {
        public string SampleName { get; set; }
        public string ParamName { get; set; }
        public List<decimal> ParamValues { get; set; }
        public List<Sample> Triggers { get; set; }
    }

    public class Attributes
    {
        public string PName { get; set; }
        public decimal Nominal { get; set; }
        public decimal PTolerence { get; set; }
        public decimal NTolerence { get; set; }
        public bool IncludeInReport { get; set; }
    }
}
