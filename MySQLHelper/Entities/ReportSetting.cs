﻿using MySQLHelper.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MySQLHelper.Entities
{
    public class ReportSetting : Entity<ReportSetting>
    {
        public enum ReportGenerationTypes
        {
            None = 0,
            EveryTrigger = 1,
            EveryShift = 2,
            DailyReport = 3,
            TimeInterval = 4
        }

        public enum ReportFormatTypes
        {
            Pdf = 0,
            Excel = 1,
            Word = 2,
            Text = 3
        }

        public enum ShiftTypes
        {
            Shift1,
            Shift2,
            Shift3
        }

        [Identity]
        public int ReportSettingID { get; set; }
        public int ReportGenerationType { get; set; }
        public int ReportFormatType { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public bool GenerateReportEnabled { get; set; }
        public int ShiftType { get; set; }
        public DateTime ShiftStartTime { get; set; }
        public DateTime ShiftEndTime { get; set; }
        public string ReportFilePath { get; set; }

        public ReportSetting()
        {
            StartDateTime = new DateTime(1900, 01, 01);
            EndDateTime = new DateTime(1900, 01, 01);
            ShiftStartTime = new DateTime(1900, 01, 01);
            ShiftEndTime = new DateTime(1900, 01, 01);
        }

        public void AddUpdateReportSetting()
        {
            try
            {
                if (ReportSettingID == 0)
                {
                    base.Add();
                }
                else
                {
                    base.Update();
                }
            }
            catch { }
        }

        public ReportSetting GetReportSetting()
        {
            ReportSetting reportSetting = new ReportSetting();
            try
            {
                string sSQL = "SELECT * FROM ReportSetting";
                reportSetting = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).FirstOrDefault();
            }
            catch
            {
            }
            return reportSetting;
        }
    }
}
