using Cognex.VisionPro.Implementation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using SAATVIK.HARDWARE;
using MySQLHelper.Entities;
using System.Threading;

namespace VisionControl
{
    partial class VisionControl
    {
        ///////////////////////// START WIZARD GENERATED
        // cognex.wizard.globals.begin
        private static string mVppFilename = @"vpp/program.vpp";
        private static string defaultResultsPath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Results");
        private static string templatePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Template.xlsx");
        public static string mResultsPath = defaultResultsPath;
        private const string mApplicationName = "Stopper";
        private static bool mUsePasswords = true;
        private static bool mQuickBuildAccess = true;
        private const string mDefaultAdministratorPassword = "admin";
        private const string mDefaultSupervisorPassword = "";
        private static DateTime mGenerationDateTime = new DateTime(2015, 7, 26, 8, 22, 27);
        private static string mGeneratedByVersion = "53.2.0.0";
        // cognex.wizard.globals.end
        ///////////////////////// END WIZARD GENERATED

        private void Wizard_FormLoad()
        {

            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.formloadactions
            ///////////////////////// END WIZARD GENERATED
        }

        private void Wizard_AttachPropertyProviders()
        {
            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.attachpropertyproviders
            ///////////////////////// END WIZARD GENERATED
        }

        private void Wizard_DetachPropertyProviders()
        {
            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.detachpropertyproviders
            ///////////////////////// END WIZARD GENERATED
        }

        private void Wizard_EnableControls(bool running)
        {
            ///////////////////////// START WIZARD GENERATED
            // cognex.wizard.enablecontrols
            ///////////////////////// END WIZARD GENERATED
        }

        private void Wizard_AddJobTabs(System.Collections.ArrayList newPagesList)
        {
            ///////////////////////// START WIZARD GENERATED
            // begin cognex.wizard.addjobtabs

            // end cognex.wizard.addjobtabs
            ///////////////////////// END WIZARD GENERATED
        }


        private void Wizard_UpdateJobResults(int idx, Cognex.VisionPro.ICogRecord result)
        {
            try
            {
                //Thread thread = new Thread(() => DumpDataFromDataWyler(result));
               // thread.Start();
                DumpDataFromDataWyler(result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void DumpDataFromDataWyler(Cognex.VisionPro.ICogRecord result)
        {
            var reportSetting = new ReportSetting().GetReportSetting();
            if (result.SubRecords.Count > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("SampleName", typeof(string));
                dt.Columns.Add("ParameterName", typeof(string));
                dt.Columns.Add("ParameterValue", typeof(decimal));
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("ProgramName", typeof(string));
                dt.Columns.Add("TrigerNumber", typeof(int));
                dt.Columns.Add("WorkStationNo", typeof(int));
                dt.Columns.Add("IsError", typeof(string));

                //p_status_Dimension
                for (int i = 1; i < result.SubRecords.Count; i++)
                {
                    var rec = result.SubRecords[i];
                    if (rec.RecordKey.StartsWith("P_Status"))
                    {
                        dt.Rows[dt.Rows.Count - 1]["Status"] = Utility.GetUserResultData(result.SubRecords[i], false);
                    }
                    else if (rec.RecordKey.StartsWith("P_Error"))
                    {
                        var errorStatus = Utility.GetUserResultData(result.SubRecords[i], false);
                        dt.Rows[dt.Rows.Count - 1]["IsError"] = (!string.IsNullOrEmpty(errorStatus) && errorStatus.Equals("Error")) ? "1" : "0";
                    }
                    else if (rec.RecordKey.StartsWith("P"))
                    {
                        var postedData = rec.RecordKey.Split('_');
                        var dimension = postedData[1];
                        decimal value = 0;
                        Decimal.TryParse(Utility.GetUserResultData(result.SubRecords[i], false), out value);
                        dt.Rows.Add(mVppFilename, dimension, value, string.Empty, mVppFilename, new Device().ReadHoldingRegisters(4304, 2)[0], new Device().ReadHoldingRegisters(4306, 2)[0]);
                    }
                }

                new Utility().AddRecordsToDB(dt);

                var currentDate = DateTime.Now;
                if (reportSetting != null)
                {
                    if (reportSetting.ReportGenerationType == ReportSetting.ReportGenerationTypes.EveryShift.GetHashCode())
                    {
                        new Utility().GenerateReport(mVppFilename, reportSetting.ShiftStartTime, reportSetting.ShiftEndTime);
                    }
                    else if (reportSetting.ReportGenerationType == ReportSetting.ReportGenerationTypes.TimeInterval.GetHashCode())
                    {
                        new Utility().GenerateReport(mVppFilename, reportSetting.StartDateTime, reportSetting.EndDateTime);
                    }
                    else if (reportSetting.ReportGenerationType == ReportSetting.ReportGenerationTypes.DailyReport.GetHashCode() && currentDate.CompareTo(new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 23, 58, 00)) > 0)
                    {
                        new Utility().GenerateReport(mVppFilename, currentDate.Date, currentDate.Date.AddDays(1).AddSeconds(-1));
                    }
                }
            }
        }

        private void GenerateReport()
        {
            new Utility().GenerateReport(mVppFilename, new DateTime(1900, 01, 01), new DateTime(2020, 01, 01));
        }
    }
}
