﻿using MySQLHelper.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisionControl
{
    public partial class ToleranceValues : Form
    {
        Dictionary<string, Attributes> attributeDictionary = new Dictionary<string, Attributes>();
        List<string> parameterNames = new List<string>();
        public ToleranceValues()
        {
        }

        public ToleranceValues(string programName)
        {
            try
            {
                var enabled = Utility.CurrentAccessLevel == AccessLevel.Administrator;
                InitializeComponent();
                int pointX = 40;
                int pointY = 40;
                var paramAttributes = new ParamAttribute().GetAll();
                var samples = new MySQLHelper.Entities.Sample().GetSamples(new DateTime(1900, 01, 01), new DateTime(2020, 01, 01), programName);
                parameterNames = samples.Select(s => s.ParameterName).Distinct().ToList();
                int adj = 15;

                Label nlbl = new Label();
                nlbl.Text = "Nominal";
                nlbl.Location = new Point(310, pointY);
                nlbl.Font = new Font(FontFamily.GenericMonospace, 7f);
                panelContent.Controls.Add(nlbl);

                Label pTlbl = new Label();
                pTlbl.Text = "+ Tolerence";
                pTlbl.Font = new Font(FontFamily.GenericMonospace, 7f);
                pTlbl.Location = new Point(nlbl.Location.X + nlbl.Width, pointY);
                panelContent.Controls.Add(pTlbl);


                Label nTlbl = new Label();
                nTlbl.Text = "-Tolerence";
                nTlbl.Font = new Font(FontFamily.GenericMonospace, 7f);
                nTlbl.Location = new Point(pTlbl.Location.X + pTlbl.Width, pointY);
                panelContent.Controls.Add(nTlbl);

                pointY += 25;

                parameterNames.ForEach(p =>
                {
                    var data = paramAttributes.FindAll(pA => pA.ParamName.Equals(p)).ToList();

                    CheckBox chkBox = new CheckBox();
                    chkBox.Text = "";
                    chkBox.Width = 20;
                    chkBox.Checked = data.Any(d => d.IncludeInReport);
                    chkBox.Name = "CheckBox_" + p;
                    chkBox.Location = new Point(pointX, pointY);
                    panelContent.Controls.Add(chkBox);

                    Label lbl = new Label();
                    lbl.Text = p;
                    lbl.Name = "Pname_" + p;
                    lbl.Width = 250;
                    lbl.TextAlign = ContentAlignment.MiddleLeft;
                    lbl.Location = new Point(pointX + 20, pointY);
                    panelContent.Controls.Add(lbl);

                    TextBox Nominal = new TextBox();
                    Nominal.Name = "Nominal_" + p;
                    Nominal.Location = new Point(lbl.Location.X + lbl.Width + adj, pointY);
                    Nominal.Width = 40;
                    var db = data.Find(d => d.AttributeName.Equals("Nominal"));
                    if (db != null)
                    {
                        Nominal.Text = db.AttributeValue.ToString();
                    }
                    Nominal.Enabled = enabled;

                    TextBox pTolerence = new TextBox();
                    pTolerence.Name = "pTolerence_" + p;
                    pTolerence.Location = new Point(Nominal.Location.X + Nominal.Width + adj + 50, pointY);
                    pTolerence.Width = 40;
                    db = data.Find(d => d.AttributeName.Equals("PTolerence"));
                    if (db != null)
                    {
                        pTolerence.Text = db.AttributeValue.ToString();
                    }
                    pTolerence.Enabled = enabled;

                    TextBox nTolerence = new TextBox();
                    nTolerence.Name = "nTolerence_" + p;
                    nTolerence.Location = new Point(pTolerence.Location.X + pTolerence.Width + adj + 50, pointY);
                    nTolerence.Width = 40;
                    db = data.Find(d => d.AttributeName.Equals("NTolerence"));
                    if (db != null)
                    {
                        nTolerence.Text = db.AttributeValue.ToString();
                    }
                    nTolerence.Enabled = enabled;

                    panelContent.Controls.Add(Nominal);
                    panelContent.Controls.Add(pTolerence);
                    panelContent.Controls.Add(nTolerence);
                    pointY += 40;
                });


            }
            catch (Exception ex)
            {

            }
        }

        private void SetAccordingToAccessLevel()
        {
            if (Utility.CurrentAccessLevel == AccessLevel.Administrator)
            {

            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            parameterNames.ForEach(p =>
            {
                //panelContent.Controls.Find("Panel_O D Meter", false).First().Controls.Find("pName", false)
                var chkBox = (CheckBox)panelContent.Controls.Find("CheckBox_" + p, false).FirstOrDefault();
                var label = panelContent.Controls.Find("Pname_" + p, false).FirstOrDefault();
                var nominal = panelContent.Controls.Find("Nominal_" + p, false).FirstOrDefault();
                var pTolerence = panelContent.Controls.Find("pTolerence_" + p, false).FirstOrDefault();
                var nTolerence = panelContent.Controls.Find("nTolerence_" + p, false).FirstOrDefault();

                if (!attributeDictionary.ContainsKey(label.Text))
                {
                    attributeDictionary.Add(label.Text, new Attributes()
                    {
                        PName = p,
                        Nominal = Convert.ToDecimal(nominal.Text != string.Empty ? nominal.Text : "0"),
                        PTolerence = Convert.ToDecimal(pTolerence.Text != string.Empty ? pTolerence.Text : "0"),
                        NTolerence = Convert.ToDecimal(nTolerence.Text != string.Empty ? nTolerence.Text : "0"),
                        IncludeInReport = chkBox.Checked
                    });
                }
            });

            new Utility().SetAttrributeValues(attributeDictionary);
            this.Close();
        }
    }
}
