﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using Modbus.Device;
using System.Threading;

namespace SAATVIK.HARDWARE
{
    public class Device
    {
        public SerialPort port = new SerialPort("Com1", 9600, Parity.Even, 7, StopBits.One);
        byte slaveID = 1;

        public Device()
        {

        }

        private static Object thisLock = new Object();
        public SerialPort Openport()
        {
            lock (thisLock)
            {
                if (port.IsOpen) port.Close();
                port.PortName = "Com1";
                port.BaudRate = 9600;
                port.DataBits = 7;
                port.Parity = Parity.Even;
                port.StopBits = StopBits.One;
                port.Open();
                return port;
            }
        }

        public IModbusSerialMaster Master()
        {
            SerialPort port = Openport();
            return ModbusSerialMaster.CreateAscii(port);
        }

        public void WriteHoldingRegisters(ushort startAddress, ushort[] registers)
        {
            IModbusSerialMaster master = Master();
            // write into holding registers
            master.WriteMultipleRegisters(slaveID, startAddress, registers);
            master.Dispose();
            port.Close();
        }

        public ushort[] ReadHoldingRegisters(ushort startAddress, ushort numRegisters)
        {
            try
            {
                lock (thisLock)
                {
                    SerialPort port = new SerialPort("Com1", 9600, Parity.Even, 7, StopBits.One);
                    if (port.IsOpen) port.Close();
                    port.PortName = "Com1";
                    port.BaudRate = 9600;
                    port.DataBits = 7;
                    port.Parity = Parity.Even;
                    port.StopBits = StopBits.One;
                    port.Open();

                    IModbusSerialMaster master = ModbusSerialMaster.CreateAscii(port);
                    ushort[] registerValues = master.ReadHoldingRegisters(slaveID, startAddress, numRegisters);
                    master.Dispose();
                    port.Close();
                    return registerValues;
                }
            }
            catch
            {
                Thread.Sleep(5000);
                return ReadHoldingRegisters(startAddress, numRegisters);
            }
        }
        public void WriteCoils(ushort startAddress, bool[] registers)
        {
            IModbusSerialMaster master = Master();

            // write coils
            master.WriteMultipleCoils(slaveID, startAddress, registers);
            master.Dispose();
            port.Close();
        }

        public bool[] ReadInputs(ushort startAddress, ushort numInputs)
        {
            IModbusSerialMaster master = Master();

            // Read inputs
            bool[] inputValues = master.ReadInputs(slaveID, startAddress, numInputs);
            master.Dispose();
            port.Close();
            return inputValues;
        }

    }
}
