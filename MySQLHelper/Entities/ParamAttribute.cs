﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySQLHelper.Global;
using System.Data;

namespace MySQLHelper.Entities
{
    public class ParamAttribute : Entity<ParamAttribute>
    {
        public string ParamName { get; set; }
        public int ParamAttributeID { get; set; }
        public string AttributeName { get; set; }
        public decimal AttributeValue { get; set; }
        public bool IncludeInReport { get; set; }

        public void Add()
        {
            try
            {
                base.Add();
            }
            catch { }
        }

        public void DeleteAll()
        {
            try
            {
                string sSQL = "DELETE FROM ParamAttribute";
                ExecuteNonQuery(sSQL);
            }
            catch { }
        }

        public List<ParamAttribute> GetAll()
        {
            List<ParamAttribute> paramAttributes = new List<ParamAttribute>();
            try
            {
                var sSQL = "SELECT * FROM ParamAttribute";
                paramAttributes = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).ToList();
            }
            catch { }
            return paramAttributes;
        }
    }


}
