﻿using MySQLHelper.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MySQLHelper.Entities
{
    public class ProductionParam : Entity<ProductionParam>
    {
        [Identity]
        public int ProductionParamID { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public string Variant { get; set; }
        public string BatchNumber { get; set; }
        public string LotNumber { get; set; }
        public string ProgramName { get; set; }

        public void Add()
        {
            try
            {
                base.Add();
            }
            catch { }
        }

        public void Update()
        {
            try
            {
                base.Update();
            }
            catch { }
        }

        public ProductionParam GetProductionParams(string progName)
        {
            ProductionParam productionParam = null;
            try
            {
                string sSQL = "SELECT * FROM ProductionParam WHERE ProgramName = '" + progName + "'";
                productionParam = ExecuteDataTable(sSQL).AsEnumerable().Select(dr => Get(dr)).FirstOrDefault();
            }
            catch
            {

            }
            return productionParam;
        }

        public void AddUpdateProductionParam(string progName)
        {
            try
            {
                var productionParam = GetProductionParams(progName);
                if (productionParam != null)
                {
                    productionParam.Update();
                }
                else
                {
                    productionParam.Add();
                }
            }
            catch
            {
            }
        }
    }
}
