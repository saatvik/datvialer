/*
SQLyog Trial v12.2.1 (32 bit)
MySQL - 5.7.11-log : Database - reportdata
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`reportdata` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `reportdata`;

/*Table structure for table `paramattribute` */

DROP TABLE IF EXISTS `paramattribute`;

CREATE TABLE `paramattribute` (
  `ParamAttributeID` int(11) NOT NULL AUTO_INCREMENT,
  `ParamName` varchar(100) NOT NULL DEFAULT '',
  `AttributeName` varchar(100) NOT NULL DEFAULT '',
  `AttributeValue` decimal(10,3) NOT NULL DEFAULT '0.000',
  `IncludeInReport` tinyint(1) NOT NULL DEFAULT '0',
  `ModifiedOn` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ModifiedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ParamAttributeID`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

/*Data for the table `paramattribute` */

insert  into `paramattribute`(`ParamAttributeID`,`ParamName`,`AttributeName`,`AttributeValue`,`IncludeInReport`,`ModifiedOn`,`ModifiedBy`) values 
(139,'Body outer diameter','Nominal','10.200',1,'2016-02-29 07:05:24',0),
(140,'Body outer diameter','PTolerence','10.300',1,'2016-02-29 07:05:24',0),
(141,'Body outer diameter','NTolerence','10.400',1,'2016-02-29 07:05:24',0),
(142,'Neck body diameter','Nominal','11.200',0,'2016-02-29 07:05:24',0),
(143,'Neck body diameter','PTolerence','11.300',0,'2016-02-29 07:05:24',0),
(144,'Neck body diameter','NTolerence','11.400',0,'2016-02-29 07:05:24',0),
(145,'O D Meter','Nominal','12.200',1,'2016-02-29 07:05:24',0),
(146,'O D Meter','PTolerence','12.300',1,'2016-02-29 07:05:24',0),
(147,'O D Meter','NTolerence','12.400',1,'2016-02-29 07:05:24',0),
(148,'L D Meter','Nominal','13.200',1,'2016-02-29 07:05:24',0),
(149,'L D Meter','PTolerence','13.300',1,'2016-02-29 07:05:24',0),
(150,'L D Meter','NTolerence','13.400',1,'2016-02-29 07:05:24',0),
(151,'Neck body diameter 1','Nominal','0.000',0,'2016-02-29 07:05:24',0),
(152,'Neck body diameter 1','PTolerence','0.000',0,'2016-02-29 07:05:24',0),
(153,'Neck body diameter 1','NTolerence','0.000',0,'2016-02-29 07:05:24',0),
(154,'Neck body diameter 2','Nominal','0.000',0,'2016-02-29 07:05:25',0),
(155,'Neck body diameter 2','PTolerence','0.000',0,'2016-02-29 07:05:25',0),
(156,'Neck body diameter 2','NTolerence','0.000',0,'2016-02-29 07:05:25',0),
(157,'Neck body diameter 3','Nominal','0.000',0,'2016-02-29 07:05:25',0),
(158,'Neck body diameter 3','PTolerence','0.000',0,'2016-02-29 07:05:25',0),
(159,'Neck body diameter 3','NTolerence','0.000',0,'2016-02-29 07:05:25',0),
(160,'Neck body diameter 4','Nominal','0.000',0,'2016-02-29 07:05:25',0),
(161,'Neck body diameter 4','PTolerence','0.000',0,'2016-02-29 07:05:25',0),
(162,'Neck body diameter 4','NTolerence','0.000',0,'2016-02-29 07:05:25',0);

/*Table structure for table `reportsetting` */

DROP TABLE IF EXISTS `reportsetting`;

CREATE TABLE `reportsetting` (
  `ReportSettingID` int(11) NOT NULL AUTO_INCREMENT,
  `ReportGenerationType` int(11) NOT NULL DEFAULT '0',
  `ReportFormatType` int(11) NOT NULL DEFAULT '0',
  `StartDateTime` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `EndDateTime` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `GenerateReportEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `ShiftType` int(11) NOT NULL DEFAULT '0',
  `ShiftStartTime` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ShiftEndTime` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ReportFilePath` varchar(500) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL DEFAULT '',
  `ModifiedOn` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ModifiedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ReportSettingID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `reportsetting` */

insert  into `reportsetting`(`ReportSettingID`,`ReportGenerationType`,`ReportFormatType`,`StartDateTime`,`EndDateTime`,`GenerateReportEnabled`,`ShiftType`,`ShiftStartTime`,`ShiftEndTime`,`ReportFilePath`,`ModifiedOn`,`ModifiedBy`) values 
(3,4,0,'1900-01-01 00:00:00','1900-01-01 00:00:00',1,2,'2016-02-27 09:12:23','2016-02-27 14:12:23','D:|','2016-02-27 09:12:43',0);

/*Table structure for table `sample` */

DROP TABLE IF EXISTS `sample`;

CREATE TABLE `sample` (
  `SampleID` int(11) NOT NULL AUTO_INCREMENT,
  `SampleName` varchar(200) NOT NULL DEFAULT '',
  `ParameterName` varchar(200) NOT NULL DEFAULT '',
  `TriggerNumber` int(11) NOT NULL DEFAULT '0',
  `ParameterValue` decimal(10,0) NOT NULL DEFAULT '0',
  `WorkStationNo` int(11) NOT NULL DEFAULT '0',
  `ModifiedOn` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ModifiedBy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SampleID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `sample` */

insert  into `sample`(`SampleID`,`SampleName`,`ParameterName`,`TriggerNumber`,`ParameterValue`,`WorkStationNo`,`ModifiedOn`,`ModifiedBy`) values 
(1,'sample 1','Body outer diameter',0,'10',0,'1900-01-01 00:00:00',0),
(2,'sample 1','Neck body diameter',0,'11',0,'1900-01-01 00:00:00',0),
(3,'sample 1','O D Meter',0,'10',0,'1900-01-01 00:00:00',0),
(4,'sample 1','L D Meter',0,'20',0,'1900-01-01 00:00:00',0),
(5,'sample 1','L D Meter',0,'20',0,'1900-01-01 00:00:00',0),
(6,'sample 1','Neck body diameter 1',0,'11',0,'1900-01-01 00:00:00',0),
(7,'sample 1','Neck body diameter 2',0,'11',0,'1900-01-01 00:00:00',0),
(8,'sample 1','Neck body diameter 3',0,'11',0,'1900-01-01 00:00:00',0),
(9,'sample 1','Neck body diameter 4',0,'11',0,'1900-01-01 00:00:00',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
