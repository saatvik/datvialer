using System.Windows.Forms;

namespace VisionControl
{
  partial class VisionControl
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        if (mJM != null)
          mJM.Shutdown();
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    // cognex.wizard.initializecomponent
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisionControl));
            this.label_controlErrorMessage = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl_ProductionParams = new System.Windows.Forms.TabControl();
            this.tabPage_JobN_JobStatistics = new System.Windows.Forms.TabPage();
            this.button_ResetStatistics = new System.Windows.Forms.Button();
            this.button_ResetStatisticsForAllJobs = new System.Windows.Forms.Button();
            this.groupBox_AcquisitionResults = new System.Windows.Forms.GroupBox();
            this.textBox_JobN_TotalAcquisitionOverruns = new System.Windows.Forms.TextBox();
            this.label_AcquisitionResults_Overruns = new System.Windows.Forms.Label();
            this.textBox_JobN_TotalAcquisitionErrors = new System.Windows.Forms.TextBox();
            this.label_AcquisitionResults_Errors = new System.Windows.Forms.Label();
            this.textBox_JobN_TotalAcquisitions = new System.Windows.Forms.TextBox();
            this.label_AcquisitionResults_TotalAcquisitions = new System.Windows.Forms.Label();
            this.groupBox_JobThroughput = new System.Windows.Forms.GroupBox();
            this.label_JobThroughput_persec = new System.Windows.Forms.Label();
            this.textBox_JobN_MaxThroughput = new System.Windows.Forms.TextBox();
            this.textBox_JobN_MinThroughput = new System.Windows.Forms.TextBox();
            this.label_JobThroughput_Max = new System.Windows.Forms.Label();
            this.label_JobThroughput_Min = new System.Windows.Forms.Label();
            this.textBox_JobN_Throughput = new System.Windows.Forms.TextBox();
            this.label_JobThroughput_TotalThroughput = new System.Windows.Forms.Label();
            this.groupBox_JobResults = new System.Windows.Forms.GroupBox();
            this.label_JobResults_Percent = new System.Windows.Forms.Label();
            this.textBox_JobN_TotalAccept_Percent = new System.Windows.Forms.TextBox();
            this.textBox_JobN_TotalWarning = new System.Windows.Forms.TextBox();
            this.textBox_JobN_TotalError = new System.Windows.Forms.TextBox();
            this.label_JobResults_Error = new System.Windows.Forms.Label();
            this.label_JobResults_Warning = new System.Windows.Forms.Label();
            this.textBox_JobN_TotalReject = new System.Windows.Forms.TextBox();
            this.label_JobResults_Reject = new System.Windows.Forms.Label();
            this.textBox_JobN_TotalAccept = new System.Windows.Forms.TextBox();
            this.label_JobResults_Accept = new System.Windows.Forms.Label();
            this.textBox_JobN_TotalIterations = new System.Windows.Forms.TextBox();
            this.label_JobResults_TotalIterations = new System.Windows.Forms.Label();
            this.tabPage_Configuration = new System.Windows.Forms.TabPage();
            this.button_Configuration = new System.Windows.Forms.Button();
            this.comboBox_Login = new System.Windows.Forms.ComboBox();
            this.label_Login = new System.Windows.Forms.Label();
            this.tabReport = new System.Windows.Forms.TabPage();
            this.btnSetValues = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_setResultsPath = new System.Windows.Forms.Button();
            this.txt_results_path = new System.Windows.Forms.TextBox();
            this.btnGenarateFromDB = new System.Windows.Forms.Button();
            this.btnProductionParams = new System.Windows.Forms.Button();
            this.panelReportRange = new System.Windows.Forms.Panel();
            this.radioDailyReport = new System.Windows.Forms.RadioButton();
            this.radioEveryTrigger = new System.Windows.Forms.RadioButton();
            this.btnSaveReportSettings = new System.Windows.Forms.Button();
            this.chkGenerateReport = new System.Windows.Forms.CheckBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_Vpp_Files = new System.Windows.Forms.ComboBox();
            this.btn_loadvppfile = new System.Windows.Forms.Button();
            this.label_Online = new System.Windows.Forms.Label();
            this.btnRunCont = new System.Windows.Forms.Button();
            this.lbl_ProgramFile = new System.Windows.Forms.PictureBox();
            this.applicationErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label_ResultBar = new System.Windows.Forms.Label();
            this.cogRecordsDisplay1 = new Cognex.VisionPro.CogRecordsDisplay();
            this.fldrBrowser_ResultsPath = new System.Windows.Forms.FolderBrowserDialog();
            this.btnResetSQLData = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.tabControl_ProductionParams.SuspendLayout();
            this.tabPage_JobN_JobStatistics.SuspendLayout();
            this.groupBox_AcquisitionResults.SuspendLayout();
            this.groupBox_JobThroughput.SuspendLayout();
            this.groupBox_JobResults.SuspendLayout();
            this.tabPage_Configuration.SuspendLayout();
            this.tabReport.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panelReportRange.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_ProgramFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationErrorProvider)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_controlErrorMessage
            // 
            this.label_controlErrorMessage.Location = new System.Drawing.Point(3, 199);
            this.label_controlErrorMessage.Name = "label_controlErrorMessage";
            this.label_controlErrorMessage.Size = new System.Drawing.Size(294, 19);
            this.label_controlErrorMessage.TabIndex = 30;
            this.label_controlErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_controlErrorMessage.Click += new System.EventHandler(this.label_controlErrorMessage_Click);
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(157, 156);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(134, 40);
            this.btnRun.TabIndex = 43;
            this.btnRun.Text = "Manual";
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.label_controlErrorMessage);
            this.panel3.Controls.Add(this.tabControl_ProductionParams);
            this.panel3.Controls.Add(this.btnRun);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.btnRunCont);
            this.panel3.Controls.Add(this.lbl_ProgramFile);
            this.panel3.Location = new System.Drawing.Point(613, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(300, 688);
            this.panel3.TabIndex = 37;
            // 
            // tabControl_ProductionParams
            // 
            this.tabControl_ProductionParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl_ProductionParams.Controls.Add(this.tabPage_JobN_JobStatistics);
            this.tabControl_ProductionParams.Controls.Add(this.tabPage_Configuration);
            this.tabControl_ProductionParams.Controls.Add(this.tabReport);
            this.tabControl_ProductionParams.Location = new System.Drawing.Point(0, 221);
            this.tabControl_ProductionParams.Name = "tabControl_ProductionParams";
            this.tabControl_ProductionParams.SelectedIndex = 0;
            this.tabControl_ProductionParams.Size = new System.Drawing.Size(302, 447);
            this.tabControl_ProductionParams.TabIndex = 28;
            this.tabControl_ProductionParams.Tag = "";
            // 
            // tabPage_JobN_JobStatistics
            // 
            this.tabPage_JobN_JobStatistics.AutoScroll = true;
            this.tabPage_JobN_JobStatistics.Controls.Add(this.button_ResetStatistics);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.button_ResetStatisticsForAllJobs);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.groupBox_AcquisitionResults);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.groupBox_JobThroughput);
            this.tabPage_JobN_JobStatistics.Controls.Add(this.groupBox_JobResults);
            this.tabPage_JobN_JobStatistics.Location = new System.Drawing.Point(4, 22);
            this.tabPage_JobN_JobStatistics.Name = "tabPage_JobN_JobStatistics";
            this.tabPage_JobN_JobStatistics.Size = new System.Drawing.Size(294, 421);
            this.tabPage_JobN_JobStatistics.TabIndex = 0;
            this.tabPage_JobN_JobStatistics.Text = "Job Statistics";
            // 
            // button_ResetStatistics
            // 
            this.button_ResetStatistics.Location = new System.Drawing.Point(35, 376);
            this.button_ResetStatistics.Name = "button_ResetStatistics";
            this.button_ResetStatistics.Size = new System.Drawing.Size(133, 23);
            this.button_ResetStatistics.TabIndex = 41;
            this.button_ResetStatistics.Text = "Reset Statistics";
            this.button_ResetStatistics.Click += new System.EventHandler(this.button_ResetStatistics_Click);
            // 
            // button_ResetStatisticsForAllJobs
            // 
            this.button_ResetStatisticsForAllJobs.Location = new System.Drawing.Point(192, 376);
            this.button_ResetStatisticsForAllJobs.Name = "button_ResetStatisticsForAllJobs";
            this.button_ResetStatisticsForAllJobs.Size = new System.Drawing.Size(168, 23);
            this.button_ResetStatisticsForAllJobs.TabIndex = 40;
            this.button_ResetStatisticsForAllJobs.Text = "Reset Statistics For All Jobs";
            this.button_ResetStatisticsForAllJobs.Click += new System.EventHandler(this.button_ResetStatisticsForAllJobs_Click);
            // 
            // groupBox_AcquisitionResults
            // 
            this.groupBox_AcquisitionResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_AcquisitionResults.Controls.Add(this.textBox_JobN_TotalAcquisitionOverruns);
            this.groupBox_AcquisitionResults.Controls.Add(this.label_AcquisitionResults_Overruns);
            this.groupBox_AcquisitionResults.Controls.Add(this.textBox_JobN_TotalAcquisitionErrors);
            this.groupBox_AcquisitionResults.Controls.Add(this.label_AcquisitionResults_Errors);
            this.groupBox_AcquisitionResults.Controls.Add(this.textBox_JobN_TotalAcquisitions);
            this.groupBox_AcquisitionResults.Controls.Add(this.label_AcquisitionResults_TotalAcquisitions);
            this.groupBox_AcquisitionResults.Location = new System.Drawing.Point(22, 160);
            this.groupBox_AcquisitionResults.Name = "groupBox_AcquisitionResults";
            this.groupBox_AcquisitionResults.Size = new System.Drawing.Size(2643, 96);
            this.groupBox_AcquisitionResults.TabIndex = 39;
            this.groupBox_AcquisitionResults.TabStop = false;
            this.groupBox_AcquisitionResults.Text = "Acquisition Results";
            // 
            // textBox_JobN_TotalAcquisitionOverruns
            // 
            this.textBox_JobN_TotalAcquisitionOverruns.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalAcquisitionOverruns.Location = new System.Drawing.Point(2410, 72);
            this.textBox_JobN_TotalAcquisitionOverruns.Name = "textBox_JobN_TotalAcquisitionOverruns";
            this.textBox_JobN_TotalAcquisitionOverruns.ReadOnly = true;
            this.textBox_JobN_TotalAcquisitionOverruns.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalAcquisitionOverruns.TabIndex = 33;
            this.textBox_JobN_TotalAcquisitionOverruns.Text = "textBox2";
            this.textBox_JobN_TotalAcquisitionOverruns.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_AcquisitionResults_Overruns
            // 
            this.label_AcquisitionResults_Overruns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_AcquisitionResults_Overruns.Location = new System.Drawing.Point(54, 74);
            this.label_AcquisitionResults_Overruns.Name = "label_AcquisitionResults_Overruns";
            this.label_AcquisitionResults_Overruns.Size = new System.Drawing.Size(2351, 16);
            this.label_AcquisitionResults_Overruns.TabIndex = 32;
            this.label_AcquisitionResults_Overruns.Text = "Overruns:";
            // 
            // textBox_JobN_TotalAcquisitionErrors
            // 
            this.textBox_JobN_TotalAcquisitionErrors.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalAcquisitionErrors.Location = new System.Drawing.Point(2410, 48);
            this.textBox_JobN_TotalAcquisitionErrors.Name = "textBox_JobN_TotalAcquisitionErrors";
            this.textBox_JobN_TotalAcquisitionErrors.ReadOnly = true;
            this.textBox_JobN_TotalAcquisitionErrors.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalAcquisitionErrors.TabIndex = 31;
            this.textBox_JobN_TotalAcquisitionErrors.Text = "textBox1";
            this.textBox_JobN_TotalAcquisitionErrors.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_AcquisitionResults_Errors
            // 
            this.label_AcquisitionResults_Errors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_AcquisitionResults_Errors.Location = new System.Drawing.Point(54, 50);
            this.label_AcquisitionResults_Errors.Name = "label_AcquisitionResults_Errors";
            this.label_AcquisitionResults_Errors.Size = new System.Drawing.Size(2351, 16);
            this.label_AcquisitionResults_Errors.TabIndex = 30;
            this.label_AcquisitionResults_Errors.Text = "Errors:";
            // 
            // textBox_JobN_TotalAcquisitions
            // 
            this.textBox_JobN_TotalAcquisitions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalAcquisitions.Location = new System.Drawing.Point(2410, 24);
            this.textBox_JobN_TotalAcquisitions.Name = "textBox_JobN_TotalAcquisitions";
            this.textBox_JobN_TotalAcquisitions.ReadOnly = true;
            this.textBox_JobN_TotalAcquisitions.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalAcquisitions.TabIndex = 28;
            this.textBox_JobN_TotalAcquisitions.Text = "textBox1";
            this.textBox_JobN_TotalAcquisitions.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_AcquisitionResults_TotalAcquisitions
            // 
            this.label_AcquisitionResults_TotalAcquisitions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_AcquisitionResults_TotalAcquisitions.Location = new System.Drawing.Point(24, 26);
            this.label_AcquisitionResults_TotalAcquisitions.Name = "label_AcquisitionResults_TotalAcquisitions";
            this.label_AcquisitionResults_TotalAcquisitions.Size = new System.Drawing.Size(2379, 16);
            this.label_AcquisitionResults_TotalAcquisitions.TabIndex = 27;
            this.label_AcquisitionResults_TotalAcquisitions.Text = "Total Acquisitions:";
            // 
            // groupBox_JobThroughput
            // 
            this.groupBox_JobThroughput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_JobThroughput.Controls.Add(this.label_JobThroughput_persec);
            this.groupBox_JobThroughput.Controls.Add(this.textBox_JobN_MaxThroughput);
            this.groupBox_JobThroughput.Controls.Add(this.textBox_JobN_MinThroughput);
            this.groupBox_JobThroughput.Controls.Add(this.label_JobThroughput_Max);
            this.groupBox_JobThroughput.Controls.Add(this.label_JobThroughput_Min);
            this.groupBox_JobThroughput.Controls.Add(this.textBox_JobN_Throughput);
            this.groupBox_JobThroughput.Controls.Add(this.label_JobThroughput_TotalThroughput);
            this.groupBox_JobThroughput.Location = new System.Drawing.Point(22, 264);
            this.groupBox_JobThroughput.Name = "groupBox_JobThroughput";
            this.groupBox_JobThroughput.Size = new System.Drawing.Size(2643, 96);
            this.groupBox_JobThroughput.TabIndex = 38;
            this.groupBox_JobThroughput.TabStop = false;
            this.groupBox_JobThroughput.Text = "Job Throughput";
            // 
            // label_JobThroughput_persec
            // 
            this.label_JobThroughput_persec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobThroughput_persec.Location = new System.Drawing.Point(2522, 26);
            this.label_JobThroughput_persec.Name = "label_JobThroughput_persec";
            this.label_JobThroughput_persec.Size = new System.Drawing.Size(96, 16);
            this.label_JobThroughput_persec.TabIndex = 29;
            this.label_JobThroughput_persec.Text = "per sec";
            // 
            // textBox_JobN_MaxThroughput
            // 
            this.textBox_JobN_MaxThroughput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_MaxThroughput.Location = new System.Drawing.Point(2410, 72);
            this.textBox_JobN_MaxThroughput.Name = "textBox_JobN_MaxThroughput";
            this.textBox_JobN_MaxThroughput.ReadOnly = true;
            this.textBox_JobN_MaxThroughput.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_MaxThroughput.TabIndex = 28;
            this.textBox_JobN_MaxThroughput.Text = "textBox1";
            this.textBox_JobN_MaxThroughput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_JobN_MinThroughput
            // 
            this.textBox_JobN_MinThroughput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_MinThroughput.Location = new System.Drawing.Point(2410, 48);
            this.textBox_JobN_MinThroughput.Name = "textBox_JobN_MinThroughput";
            this.textBox_JobN_MinThroughput.ReadOnly = true;
            this.textBox_JobN_MinThroughput.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_MinThroughput.TabIndex = 27;
            this.textBox_JobN_MinThroughput.Text = "textBox1";
            this.textBox_JobN_MinThroughput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_JobThroughput_Max
            // 
            this.label_JobThroughput_Max.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobThroughput_Max.Location = new System.Drawing.Point(54, 74);
            this.label_JobThroughput_Max.Name = "label_JobThroughput_Max";
            this.label_JobThroughput_Max.Size = new System.Drawing.Size(2351, 16);
            this.label_JobThroughput_Max.TabIndex = 26;
            this.label_JobThroughput_Max.Text = "Max:";
            // 
            // label_JobThroughput_Min
            // 
            this.label_JobThroughput_Min.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobThroughput_Min.Location = new System.Drawing.Point(54, 50);
            this.label_JobThroughput_Min.Name = "label_JobThroughput_Min";
            this.label_JobThroughput_Min.Size = new System.Drawing.Size(2351, 16);
            this.label_JobThroughput_Min.TabIndex = 25;
            this.label_JobThroughput_Min.Text = "Min:";
            // 
            // textBox_JobN_Throughput
            // 
            this.textBox_JobN_Throughput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_Throughput.Location = new System.Drawing.Point(2410, 24);
            this.textBox_JobN_Throughput.Name = "textBox_JobN_Throughput";
            this.textBox_JobN_Throughput.ReadOnly = true;
            this.textBox_JobN_Throughput.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_Throughput.TabIndex = 24;
            this.textBox_JobN_Throughput.Text = "textBox1";
            this.textBox_JobN_Throughput.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_JobThroughput_TotalThroughput
            // 
            this.label_JobThroughput_TotalThroughput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobThroughput_TotalThroughput.Location = new System.Drawing.Point(24, 26);
            this.label_JobThroughput_TotalThroughput.Name = "label_JobThroughput_TotalThroughput";
            this.label_JobThroughput_TotalThroughput.Size = new System.Drawing.Size(2379, 16);
            this.label_JobThroughput_TotalThroughput.TabIndex = 23;
            this.label_JobThroughput_TotalThroughput.Text = "Total Throughput:";
            // 
            // groupBox_JobResults
            // 
            this.groupBox_JobResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_JobResults.Controls.Add(this.label_JobResults_Percent);
            this.groupBox_JobResults.Controls.Add(this.textBox_JobN_TotalAccept_Percent);
            this.groupBox_JobResults.Controls.Add(this.textBox_JobN_TotalWarning);
            this.groupBox_JobResults.Controls.Add(this.textBox_JobN_TotalError);
            this.groupBox_JobResults.Controls.Add(this.label_JobResults_Error);
            this.groupBox_JobResults.Controls.Add(this.label_JobResults_Warning);
            this.groupBox_JobResults.Controls.Add(this.textBox_JobN_TotalReject);
            this.groupBox_JobResults.Controls.Add(this.label_JobResults_Reject);
            this.groupBox_JobResults.Controls.Add(this.textBox_JobN_TotalAccept);
            this.groupBox_JobResults.Controls.Add(this.label_JobResults_Accept);
            this.groupBox_JobResults.Controls.Add(this.textBox_JobN_TotalIterations);
            this.groupBox_JobResults.Controls.Add(this.label_JobResults_TotalIterations);
            this.groupBox_JobResults.Location = new System.Drawing.Point(22, 8);
            this.groupBox_JobResults.Name = "groupBox_JobResults";
            this.groupBox_JobResults.Size = new System.Drawing.Size(2643, 144);
            this.groupBox_JobResults.TabIndex = 37;
            this.groupBox_JobResults.TabStop = false;
            this.groupBox_JobResults.Text = "Job Results";
            // 
            // label_JobResults_Percent
            // 
            this.label_JobResults_Percent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobResults_Percent.Location = new System.Drawing.Point(2578, 51);
            this.label_JobResults_Percent.Name = "label_JobResults_Percent";
            this.label_JobResults_Percent.Size = new System.Drawing.Size(34, 16);
            this.label_JobResults_Percent.TabIndex = 40;
            this.label_JobResults_Percent.Text = "%";
            // 
            // textBox_JobN_TotalAccept_Percent
            // 
            this.textBox_JobN_TotalAccept_Percent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalAccept_Percent.Location = new System.Drawing.Point(2526, 48);
            this.textBox_JobN_TotalAccept_Percent.Name = "textBox_JobN_TotalAccept_Percent";
            this.textBox_JobN_TotalAccept_Percent.ReadOnly = true;
            this.textBox_JobN_TotalAccept_Percent.Size = new System.Drawing.Size(52, 20);
            this.textBox_JobN_TotalAccept_Percent.TabIndex = 39;
            this.textBox_JobN_TotalAccept_Percent.Text = "textBox1";
            this.textBox_JobN_TotalAccept_Percent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_JobN_TotalWarning
            // 
            this.textBox_JobN_TotalWarning.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalWarning.Location = new System.Drawing.Point(2410, 96);
            this.textBox_JobN_TotalWarning.Name = "textBox_JobN_TotalWarning";
            this.textBox_JobN_TotalWarning.ReadOnly = true;
            this.textBox_JobN_TotalWarning.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalWarning.TabIndex = 38;
            this.textBox_JobN_TotalWarning.Text = "textBox4";
            this.textBox_JobN_TotalWarning.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_JobN_TotalError
            // 
            this.textBox_JobN_TotalError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalError.Location = new System.Drawing.Point(2410, 120);
            this.textBox_JobN_TotalError.Name = "textBox_JobN_TotalError";
            this.textBox_JobN_TotalError.ReadOnly = true;
            this.textBox_JobN_TotalError.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalError.TabIndex = 36;
            this.textBox_JobN_TotalError.Text = "textBox3";
            this.textBox_JobN_TotalError.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_JobResults_Error
            // 
            this.label_JobResults_Error.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobResults_Error.Location = new System.Drawing.Point(54, 122);
            this.label_JobResults_Error.Name = "label_JobResults_Error";
            this.label_JobResults_Error.Size = new System.Drawing.Size(2351, 16);
            this.label_JobResults_Error.TabIndex = 35;
            this.label_JobResults_Error.Text = "Error:";
            // 
            // label_JobResults_Warning
            // 
            this.label_JobResults_Warning.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobResults_Warning.Location = new System.Drawing.Point(54, 98);
            this.label_JobResults_Warning.Name = "label_JobResults_Warning";
            this.label_JobResults_Warning.Size = new System.Drawing.Size(2351, 16);
            this.label_JobResults_Warning.TabIndex = 34;
            this.label_JobResults_Warning.Text = "Warning:";
            // 
            // textBox_JobN_TotalReject
            // 
            this.textBox_JobN_TotalReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalReject.Location = new System.Drawing.Point(2410, 72);
            this.textBox_JobN_TotalReject.Name = "textBox_JobN_TotalReject";
            this.textBox_JobN_TotalReject.ReadOnly = true;
            this.textBox_JobN_TotalReject.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalReject.TabIndex = 33;
            this.textBox_JobN_TotalReject.Text = "textBox2";
            this.textBox_JobN_TotalReject.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_JobResults_Reject
            // 
            this.label_JobResults_Reject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobResults_Reject.Location = new System.Drawing.Point(54, 74);
            this.label_JobResults_Reject.Name = "label_JobResults_Reject";
            this.label_JobResults_Reject.Size = new System.Drawing.Size(2351, 16);
            this.label_JobResults_Reject.TabIndex = 31;
            this.label_JobResults_Reject.Text = "Reject:";
            // 
            // textBox_JobN_TotalAccept
            // 
            this.textBox_JobN_TotalAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalAccept.Location = new System.Drawing.Point(2410, 48);
            this.textBox_JobN_TotalAccept.Name = "textBox_JobN_TotalAccept";
            this.textBox_JobN_TotalAccept.ReadOnly = true;
            this.textBox_JobN_TotalAccept.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalAccept.TabIndex = 29;
            this.textBox_JobN_TotalAccept.Text = "textBox1";
            this.textBox_JobN_TotalAccept.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_JobResults_Accept
            // 
            this.label_JobResults_Accept.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobResults_Accept.Location = new System.Drawing.Point(54, 50);
            this.label_JobResults_Accept.Name = "label_JobResults_Accept";
            this.label_JobResults_Accept.Size = new System.Drawing.Size(2351, 16);
            this.label_JobResults_Accept.TabIndex = 28;
            this.label_JobResults_Accept.Text = "Accept:";
            // 
            // textBox_JobN_TotalIterations
            // 
            this.textBox_JobN_TotalIterations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JobN_TotalIterations.Location = new System.Drawing.Point(2410, 24);
            this.textBox_JobN_TotalIterations.Name = "textBox_JobN_TotalIterations";
            this.textBox_JobN_TotalIterations.ReadOnly = true;
            this.textBox_JobN_TotalIterations.Size = new System.Drawing.Size(100, 20);
            this.textBox_JobN_TotalIterations.TabIndex = 26;
            this.textBox_JobN_TotalIterations.Text = "textBox1";
            this.textBox_JobN_TotalIterations.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_JobResults_TotalIterations
            // 
            this.label_JobResults_TotalIterations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_JobResults_TotalIterations.Location = new System.Drawing.Point(24, 26);
            this.label_JobResults_TotalIterations.Name = "label_JobResults_TotalIterations";
            this.label_JobResults_TotalIterations.Size = new System.Drawing.Size(2379, 16);
            this.label_JobResults_TotalIterations.TabIndex = 25;
            this.label_JobResults_TotalIterations.Text = "Total Iterations:";
            // 
            // tabPage_Configuration
            // 
            this.tabPage_Configuration.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage_Configuration.Controls.Add(this.button_Configuration);
            this.tabPage_Configuration.Controls.Add(this.comboBox_Login);
            this.tabPage_Configuration.Controls.Add(this.label_Login);
            this.tabPage_Configuration.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Configuration.Name = "tabPage_Configuration";
            this.tabPage_Configuration.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Configuration.Size = new System.Drawing.Size(294, 421);
            this.tabPage_Configuration.TabIndex = 1;
            this.tabPage_Configuration.Text = "Configuration";
            // 
            // button_Configuration
            // 
            this.button_Configuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Configuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Configuration.Location = new System.Drawing.Point(105, 55);
            this.button_Configuration.Name = "button_Configuration";
            this.button_Configuration.Size = new System.Drawing.Size(182, 37);
            this.button_Configuration.TabIndex = 45;
            this.button_Configuration.Text = "System Configuration";
            this.button_Configuration.Click += new System.EventHandler(this.button_Configuration_Click);
            // 
            // comboBox_Login
            // 
            this.comboBox_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Login.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Login.Location = new System.Drawing.Point(105, 25);
            this.comboBox_Login.Name = "comboBox_Login";
            this.comboBox_Login.Size = new System.Drawing.Size(181, 24);
            this.comboBox_Login.TabIndex = 41;
            this.comboBox_Login.SelectionChangeCommitted += new System.EventHandler(this.comboBox_Login_SelectionChangeCommitted);
            // 
            // label_Login
            // 
            this.label_Login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Login.Location = new System.Drawing.Point(3, 28);
            this.label_Login.Name = "label_Login";
            this.label_Login.Size = new System.Drawing.Size(103, 21);
            this.label_Login.TabIndex = 49;
            this.label_Login.Text = "Current login:";
            // 
            // tabReport
            // 
            this.tabReport.Controls.Add(this.btnSetValues);
            this.tabReport.Controls.Add(this.panel8);
            this.tabReport.Controls.Add(this.btnGenarateFromDB);
            this.tabReport.Controls.Add(this.btnProductionParams);
            this.tabReport.Controls.Add(this.panelReportRange);
            this.tabReport.Controls.Add(this.chkGenerateReport);
            this.tabReport.Location = new System.Drawing.Point(4, 22);
            this.tabReport.Name = "tabReport";
            this.tabReport.Padding = new System.Windows.Forms.Padding(3);
            this.tabReport.Size = new System.Drawing.Size(294, 421);
            this.tabReport.TabIndex = 2;
            this.tabReport.Text = "Report";
            this.tabReport.UseVisualStyleBackColor = true;
            // 
            // btnSetValues
            // 
            this.btnSetValues.Location = new System.Drawing.Point(136, 16);
            this.btnSetValues.Name = "btnSetValues";
            this.btnSetValues.Size = new System.Drawing.Size(121, 23);
            this.btnSetValues.TabIndex = 65;
            this.btnSetValues.Text = "Set Tolarence Values";
            this.btnSetValues.UseVisualStyleBackColor = true;
            this.btnSetValues.Click += new System.EventHandler(this.btnSetValues_Click);
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.btn_setResultsPath);
            this.panel8.Controls.Add(this.txt_results_path);
            this.panel8.Location = new System.Drawing.Point(8, 195);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(280, 55);
            this.panel8.TabIndex = 64;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Result Destination Path:";
            // 
            // btn_setResultsPath
            // 
            this.btn_setResultsPath.Location = new System.Drawing.Point(164, 4);
            this.btn_setResultsPath.Name = "btn_setResultsPath";
            this.btn_setResultsPath.Size = new System.Drawing.Size(113, 38);
            this.btn_setResultsPath.TabIndex = 1;
            this.btn_setResultsPath.Text = "Select Results Path";
            this.btn_setResultsPath.UseVisualStyleBackColor = true;
            this.btn_setResultsPath.Click += new System.EventHandler(this.btn_setResultsPath_Click_1);
            // 
            // txt_results_path
            // 
            this.txt_results_path.Enabled = false;
            this.txt_results_path.Location = new System.Drawing.Point(2, 22);
            this.txt_results_path.Name = "txt_results_path";
            this.txt_results_path.Size = new System.Drawing.Size(155, 20);
            this.txt_results_path.TabIndex = 0;
            // 
            // btnGenarateFromDB
            // 
            this.btnGenarateFromDB.Location = new System.Drawing.Point(28, 381);
            this.btnGenarateFromDB.Name = "btnGenarateFromDB";
            this.btnGenarateFromDB.Size = new System.Drawing.Size(228, 34);
            this.btnGenarateFromDB.TabIndex = 63;
            this.btnGenarateFromDB.Text = "Generate Report From Database";
            this.btnGenarateFromDB.UseVisualStyleBackColor = true;
            this.btnGenarateFromDB.Click += new System.EventHandler(this.btnGenarateFromDB_Click);
            // 
            // btnProductionParams
            // 
            this.btnProductionParams.Location = new System.Drawing.Point(7, 16);
            this.btnProductionParams.Name = "btnProductionParams";
            this.btnProductionParams.Size = new System.Drawing.Size(123, 23);
            this.btnProductionParams.TabIndex = 62;
            this.btnProductionParams.Text = "Production Parameters";
            this.btnProductionParams.UseVisualStyleBackColor = true;
            this.btnProductionParams.Click += new System.EventHandler(this.btnProductionParams_Click_1);
            // 
            // panelReportRange
            // 
            this.panelReportRange.Controls.Add(this.btnResetSQLData);
            this.panelReportRange.Controls.Add(this.radioDailyReport);
            this.panelReportRange.Controls.Add(this.radioEveryTrigger);
            this.panelReportRange.Controls.Add(this.btnSaveReportSettings);
            this.panelReportRange.Location = new System.Drawing.Point(27, 68);
            this.panelReportRange.Name = "panelReportRange";
            this.panelReportRange.Size = new System.Drawing.Size(259, 106);
            this.panelReportRange.TabIndex = 61;
            // 
            // radioDailyReport
            // 
            this.radioDailyReport.AutoSize = true;
            this.radioDailyReport.Location = new System.Drawing.Point(0, 27);
            this.radioDailyReport.Name = "radioDailyReport";
            this.radioDailyReport.Size = new System.Drawing.Size(78, 17);
            this.radioDailyReport.TabIndex = 67;
            this.radioDailyReport.TabStop = true;
            this.radioDailyReport.Text = "Daily report";
            this.radioDailyReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioDailyReport.UseVisualStyleBackColor = true;
            // 
            // radioEveryTrigger
            // 
            this.radioEveryTrigger.AutoSize = true;
            this.radioEveryTrigger.Location = new System.Drawing.Point(0, 4);
            this.radioEveryTrigger.Name = "radioEveryTrigger";
            this.radioEveryTrigger.Size = new System.Drawing.Size(112, 17);
            this.radioEveryTrigger.TabIndex = 64;
            this.radioEveryTrigger.TabStop = true;
            this.radioEveryTrigger.Text = "Every work station";
            this.radioEveryTrigger.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioEveryTrigger.UseVisualStyleBackColor = true;
            this.radioEveryTrigger.CheckedChanged += new System.EventHandler(this.radioEveryTrigger_CheckedChanged);
            // 
            // btnSaveReportSettings
            // 
            this.btnSaveReportSettings.Location = new System.Drawing.Point(63, 66);
            this.btnSaveReportSettings.Name = "btnSaveReportSettings";
            this.btnSaveReportSettings.Size = new System.Drawing.Size(75, 23);
            this.btnSaveReportSettings.TabIndex = 64;
            this.btnSaveReportSettings.Text = "Save";
            this.btnSaveReportSettings.UseVisualStyleBackColor = true;
            this.btnSaveReportSettings.Click += new System.EventHandler(this.btnSaveReportSettings_Click);
            // 
            // chkGenerateReport
            // 
            this.chkGenerateReport.AutoSize = true;
            this.chkGenerateReport.Location = new System.Drawing.Point(6, 45);
            this.chkGenerateReport.Name = "chkGenerateReport";
            this.chkGenerateReport.Size = new System.Drawing.Size(105, 17);
            this.chkGenerateReport.TabIndex = 60;
            this.chkGenerateReport.Text = "Generate Report";
            this.chkGenerateReport.UseVisualStyleBackColor = true;
            this.chkGenerateReport.CheckedChanged += new System.EventHandler(this.chkGenerateReport_CheckedChanged_1);
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.comboBox_Vpp_Files);
            this.panel7.Controls.Add(this.btn_loadvppfile);
            this.panel7.Controls.Add(this.label_Online);
            this.applicationErrorProvider.SetIconAlignment(this.panel7, System.Windows.Forms.ErrorIconAlignment.TopRight);
            this.panel7.Location = new System.Drawing.Point(-3, 89);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(300, 61);
            this.panel7.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Select Program File:";
            // 
            // comboBox_Vpp_Files
            // 
            this.comboBox_Vpp_Files.FormattingEnabled = true;
            this.comboBox_Vpp_Files.Location = new System.Drawing.Point(3, 16);
            this.comboBox_Vpp_Files.Name = "comboBox_Vpp_Files";
            this.comboBox_Vpp_Files.Size = new System.Drawing.Size(188, 21);
            this.comboBox_Vpp_Files.TabIndex = 53;
            // 
            // btn_loadvppfile
            // 
            this.btn_loadvppfile.Location = new System.Drawing.Point(197, 3);
            this.btn_loadvppfile.Name = "btn_loadvppfile";
            this.btn_loadvppfile.Size = new System.Drawing.Size(97, 37);
            this.btn_loadvppfile.TabIndex = 54;
            this.btn_loadvppfile.Text = "Set Program";
            this.btn_loadvppfile.UseVisualStyleBackColor = true;
            this.btn_loadvppfile.Click += new System.EventHandler(this.btn_loadvppfile_Click);
            // 
            // label_Online
            // 
            this.label_Online.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Online.AutoSize = true;
            this.label_Online.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Online.Location = new System.Drawing.Point(0, 40);
            this.label_Online.Name = "label_Online";
            this.label_Online.Size = new System.Drawing.Size(119, 15);
            this.label_Online.TabIndex = 51;
            this.label_Online.Text = "System online status";
            this.label_Online.Click += new System.EventHandler(this.label_Online_Click);
            // 
            // btnRunCont
            // 
            this.btnRunCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunCont.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunCont.Location = new System.Drawing.Point(4, 156);
            this.btnRunCont.Name = "btnRunCont";
            this.btnRunCont.Size = new System.Drawing.Size(133, 40);
            this.btnRunCont.TabIndex = 42;
            this.btnRunCont.Text = "Autorun";
            this.btnRunCont.Click += new System.EventHandler(this.btnRunCont_Click);
            // 
            // lbl_ProgramFile
            // 
            this.lbl_ProgramFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_ProgramFile.Image = ((System.Drawing.Image)(resources.GetObject("lbl_ProgramFile.Image")));
            this.lbl_ProgramFile.Location = new System.Drawing.Point(-3, 3);
            this.lbl_ProgramFile.Name = "lbl_ProgramFile";
            this.lbl_ProgramFile.Size = new System.Drawing.Size(300, 80);
            this.lbl_ProgramFile.TabIndex = 52;
            this.lbl_ProgramFile.TabStop = false;
            // 
            // applicationErrorProvider
            // 
            this.applicationErrorProvider.ContainerControl = this;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.cogRecordsDisplay1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(610, 694);
            this.panel1.TabIndex = 36;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.label_ResultBar);
            this.panel2.Location = new System.Drawing.Point(0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(607, 48);
            this.panel2.TabIndex = 56;
            // 
            // label_ResultBar
            // 
            this.label_ResultBar.BackColor = System.Drawing.SystemColors.Control;
            this.label_ResultBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_ResultBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ResultBar.Location = new System.Drawing.Point(0, 0);
            this.label_ResultBar.Name = "label_ResultBar";
            this.label_ResultBar.Size = new System.Drawing.Size(607, 48);
            this.label_ResultBar.TabIndex = 26;
            this.label_ResultBar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cogRecordsDisplay1
            // 
            this.cogRecordsDisplay1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cogRecordsDisplay1.Location = new System.Drawing.Point(1, 48);
            this.cogRecordsDisplay1.Name = "cogRecordsDisplay1";
            this.cogRecordsDisplay1.SelectedRecordKey = null;
            this.cogRecordsDisplay1.ShowRecordsDropDown = true;
            this.cogRecordsDisplay1.Size = new System.Drawing.Size(606, 642);
            this.cogRecordsDisplay1.Subject = null;
            this.cogRecordsDisplay1.TabIndex = 30;
            // 
            // btnResetSQLData
            // 
            this.btnResetSQLData.Location = new System.Drawing.Point(145, 66);
            this.btnResetSQLData.Name = "btnResetSQLData";
            this.btnResetSQLData.Size = new System.Drawing.Size(85, 23);
            this.btnResetSQLData.TabIndex = 68;
            this.btnResetSQLData.Text = "Reset Data";
            this.btnResetSQLData.UseVisualStyleBackColor = true;
            this.btnResetSQLData.Click += new System.EventHandler(this.btnResetSQLData_Click);
            // 
            // VisionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "VisionControl";
            this.Size = new System.Drawing.Size(913, 697);
            this.Load += new System.EventHandler(this.VisionControl_Load);
            this.panel3.ResumeLayout(false);
            this.tabControl_ProductionParams.ResumeLayout(false);
            this.tabPage_JobN_JobStatistics.ResumeLayout(false);
            this.groupBox_AcquisitionResults.ResumeLayout(false);
            this.groupBox_AcquisitionResults.PerformLayout();
            this.groupBox_JobThroughput.ResumeLayout(false);
            this.groupBox_JobThroughput.PerformLayout();
            this.groupBox_JobResults.ResumeLayout(false);
            this.groupBox_JobResults.PerformLayout();
            this.tabPage_Configuration.ResumeLayout(false);
            this.tabReport.ResumeLayout(false);
            this.tabReport.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panelReportRange.ResumeLayout(false);
            this.panelReportRange.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbl_ProgramFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationErrorProvider)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnRun;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Button btnRunCont;
    private System.Windows.Forms.Label label_Online;
    private System.Windows.Forms.ErrorProvider applicationErrorProvider;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label_controlErrorMessage;
    private PictureBox lbl_ProgramFile;
        private ComboBox comboBox_Vpp_Files;
        private Panel panel7;
        private FolderBrowserDialog fldrBrowser_ResultsPath;
        private Button btn_loadvppfile;
        private Panel panel2;
        private Label label_ResultBar;
        private Label label2;
        private Cognex.VisionPro.CogRecordsDisplay cogRecordsDisplay1;
        private TabControl tabControl_ProductionParams;
        private TabPage tabPage_JobN_JobStatistics;
        private Button button_ResetStatistics;
        private Button button_ResetStatisticsForAllJobs;
        private GroupBox groupBox_AcquisitionResults;
        private TextBox textBox_JobN_TotalAcquisitionOverruns;
        private Label label_AcquisitionResults_Overruns;
        private TextBox textBox_JobN_TotalAcquisitionErrors;
        private Label label_AcquisitionResults_Errors;
        private TextBox textBox_JobN_TotalAcquisitions;
        private Label label_AcquisitionResults_TotalAcquisitions;
        private GroupBox groupBox_JobThroughput;
        private Label label_JobThroughput_persec;
        private TextBox textBox_JobN_MaxThroughput;
        private TextBox textBox_JobN_MinThroughput;
        private Label label_JobThroughput_Max;
        private Label label_JobThroughput_Min;
        private TextBox textBox_JobN_Throughput;
        private Label label_JobThroughput_TotalThroughput;
        private GroupBox groupBox_JobResults;
        private Label label_JobResults_Percent;
        private TextBox textBox_JobN_TotalAccept_Percent;
        private TextBox textBox_JobN_TotalWarning;
        private TextBox textBox_JobN_TotalError;
        private Label label_JobResults_Error;
        private Label label_JobResults_Warning;
        private TextBox textBox_JobN_TotalReject;
        private Label label_JobResults_Reject;
        private TextBox textBox_JobN_TotalAccept;
        private Label label_JobResults_Accept;
        private TextBox textBox_JobN_TotalIterations;
        private Label label_JobResults_TotalIterations;
        private TabPage tabPage_Configuration;
        private Button button_Configuration;
        private ComboBox comboBox_Login;
        private Label label_Login;
        private TabPage tabReport;
        private Button btnGenarateFromDB;
        private Button btnProductionParams;
        private Panel panelReportRange;
        private CheckBox chkGenerateReport;
        private Button btnSaveReportSettings;
        private RadioButton radioDailyReport;
        internal RadioButton radioEveryTrigger;
        private Panel panel8;
        private Label label1;
        private Button btn_setResultsPath;
        private TextBox txt_results_path;
        private Button btnSetValues;
        private Button btnResetSQLData;
    }
}
