﻿using MySQLHelper.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisionControl
{
    public partial class ProductionParamsForm : Form
    {
        public string ProgName { get; set; }
        public ProductionParamsForm()
        {
        }

        public ProductionParamsForm(string programName)
        {
            InitializeComponent();
            ProgName = programName;
            var productionParam = new ProductionParam().GetProductionParams(programName);
            if (productionParam != null)
            {
                txtProductName.Text = productionParam.ProductName;
                txtVariant.Text = productionParam.Variant;
                txtBatchNo.Text = productionParam.BatchNumber;
                txtLotNo.Text = productionParam.LotNumber;
                txtCustomerName.Text = productionParam.CustomerName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var productionParam = new ProductionParam().GetProductionParams(ProgName);
            if (productionParam == null)
            {
                productionParam = new ProductionParam() { ProgramName = ProgName, CustomerName = txtCustomerName.Text, ProductName = txtProductName.Text, Variant = txtVariant.Text, BatchNumber = txtBatchNo.Text, LotNumber = txtLotNo.Text };
                productionParam.Add();
            }
            else
            {
                productionParam.CustomerName = txtCustomerName.Text;
                productionParam.ProductName = txtProductName.Text;
                productionParam.Variant = txtVariant.Text;
                productionParam.BatchNumber = txtBatchNo.Text;
                productionParam.LotNumber = txtLotNo.Text;
                productionParam.ProgramName = ProgName;
                productionParam.Update();
            }
            this.Close();
        }
    }
}
