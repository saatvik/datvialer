﻿using MySQLHelper.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisionControl
{
    public partial class SQLResetWarning : Form
    {
        static string ProgName = string.Empty;
        public SQLResetWarning()
        {
            InitializeComponent();
        }

        public SQLResetWarning(string PName)
        {
            ProgName = PName;
            InitializeComponent();
        }


        private void btnNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                new Sample().ResetSQLData(ProgName);
            }
            catch
            {

            }
            finally
            {
                this.Close();
            }
        }
    }
}
